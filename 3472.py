les = int(input())

start = 9 * 60
les_time = 45
sm_br = 5
b_br = 15

sm_br2 = les // 2
b_br2 = (les - 1) // 2

end = start + les_time * les + sm_br * sm_br2 + b_br * b_br2

hours = end // 60
minutes = end % 60
print("%s %s" % (hours, minutes))
